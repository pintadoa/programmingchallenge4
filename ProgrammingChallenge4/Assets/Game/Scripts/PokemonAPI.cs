﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PokemonAPI : MonoBehaviour
{
    [Serializable]
    public class PokemonSprites
    {
        public string front_default;
    }

    [Serializable]
    public class Pokemon
    {
        public int id;
        public string name;
        public PokemonSprites sprites;
    }

    private const float updateTimeDelay = 1.0f;
    private float updateDelay = 0.0f;
    public int score;

    public InputField numberText;
    public Text nameText;
    public Image pokemonImage;
    public LocationServicesController locationServicesController;

    private void Start()
    {
        numberText.text = 1.ToString();
        score = 0;
    }

    private void Update()
    {
        updateDelay -= Time.deltaTime;
        if (updateDelay <= 0.0f)
        {
            Pokemon info = GetPokemon();
            nameText.text = info.name;
            StartCoroutine(GetPokemonImage(info.sprites.front_default));
            updateDelay = updateTimeDelay;
        }
    }

    private Pokemon GetPokemon()
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
            String.Format("https://pokeapi.co/api/v2/pokemon/{0}", numberText.text)
            );

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();

        return JsonUtility.FromJson<Pokemon>(jsonResponse);
    }

    IEnumerator GetPokemonImage(string urlPokemon)
    {
        UnityWebRequest www = UnityWebRequest.Get(urlPokemon);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {

            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
            Texture2D tex = new Texture2D(pokemonImage.sprite.texture.width, pokemonImage.sprite.texture.height);
            tex.LoadImage(results);
            pokemonImage.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100f);
        }
    }

    public void OnClickCatchPokemon()
    {
        score++;
        PlayFabClientAPI.WritePlayerEvent(
           new WriteClientPlayerEventRequest()
           {
               EventName = "player_updated_pokemon_caught",
               Body = new Dictionary<string, object>() { { "name", nameText.text },
                   { "longitude", locationServicesController.longitudeText},
                   { "latitude", locationServicesController.latitudeText }
               } 
           },
           result =>
           {
               Debug.Log("Success sending event: player_updated_pokemon_caught");
           },
           error =>
           {
               Debug.Log("Failed to send event: player_updated_pokemon_caught");
           }
       );
    }

    public void PostHighScore()
    {
        PlayFabClientAPI.UpdatePlayerStatistics(
            new UpdatePlayerStatisticsRequest()
            {
                Statistics = new List<StatisticUpdate>()
                {
                    new StatisticUpdate() {StatisticName = "high_score", Value = score}
                }
            },
            OnUpdatePlayerStatisticsResponse,
            OnPlayFabError
        );
    }

    public void OnUpdatePlayerStatisticsResponse(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("User statistics updated");
    }

    public void OnPlayFabError(PlayFabError error)
    {
        Debug.Log("PlayFab Error: " + error.GenerateErrorReport());
    }

}
